Based on fuzzwork dump and official MariaDB docker image.
# Usage
```
docker run --detach --name eve-sde stevencmy/eve-sde-mysql-dockerized:latest
```
To update SDE to latest, delete container and re-run.
## ROOT PASSWORD
By default, a random password will be generated and print to logs on first startup.

> MYSQL_RANDOM_ROOT_PASSWORD
> This is an optional variable. Set to yes to generate a random initial password for the root user (using pwgen). 
> The generated root password will be printed to stdout (GENERATED ROOT PASSWORD: .....).

To use your own password:
```
docker run --detach --name eve-sde --env MARIADB_ROOT_PASSWORD=my-secret-pw stevencmy/eve-sde-mysql-dockerized:latest
```
## Docker compose
```
version: '3.1'

services:

  db:
    image: stevencmy/eve-sde-mysql-dockerized
    restart: always
    environment:
      MARIADB_ROOT_PASSWORD: example

  adminer:
    image: adminer
    restart: always
    ports:
      - 8080:8080
```
For more usage, see [MariaDb docker](https://hub.docker.com/_/mariadb).